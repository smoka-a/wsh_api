import pytest
import requests
from faker import Faker
import endpoints_cities
from models.cities import Cities

fake = Faker()

def test_get_cities_list(new_city):
    response = requests.get(endpoints_cities.city_get)
    response_dict = response.json()
    names_list = [a['name'] for a in response_dict]
    print(names_list)
    assert new_city.name in names_list

def test_create_city(new_city):
    list_params = {"name": new_city.name, "population": new_city.population}
    filtered_list_response = requests.get(endpoints_cities.city_get, params=list_params)
    assert filtered_list_response.status_code == 200
    assert new_city.name in filtered_list_response.text

def test_get_city_by_id(new_city):
    response = requests.get(f'{endpoints_cities.city_get_id}/{new_city.id}')
    city_data = response.json()
    assert city_data['name'] == new_city.name

def test_update_city(new_city):
    new_city.update_city('Toronto', 666)
    assert new_city.name == 'Toronto'

def test_delete_city(new_city):
    new_city.delete_city()
    response = requests.get(f'{endpoints_cities.city_get_id}/{new_city.id}')
    assert response.status_code == 404

@pytest.fixture
def new_city():
    city = Cities()
    city.create_city()
    return city