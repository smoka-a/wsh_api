import requests
import random

base_url = 'http://18.184.234.77:8080'

def test_add_calculator():
    first_random_number = random.randint(1,10)
    second_random_number = random.randint(1,10)
    body = {
        "firstNumber": first_random_number,
        "secondNumber": second_random_number
}

    add_calculator_response = requests.post(f'{base_url}/add', json=body)
    assert add_calculator_response.status_code == 200
    result = add_calculator_response.json()['result']    # wyciągniecie klucza ze słownika
    result_add = first_random_number + second_random_number
    assert result == result_add


