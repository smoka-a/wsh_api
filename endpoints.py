base_url = 'http://18.184.234.77:8080'

ping = f'{base_url}/ping'

calculator_add = f'{base_url}/add'

# accounts controller
accounts = f'{base_url}/accounts'
accounts_create = f'{base_url}/accounts/create'
accounts_delete = f'{base_url}/accounts/delete'
