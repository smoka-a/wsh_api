import requests
from faker import Faker
import random
import endpoints_cities

fake = Faker()

class Cities:
    def __init__(self):
        self.name = fake.uuid4()
        self.population = random.randint(1, 1000)
        self.id = None

    def get_cities_list(self):
        get_list_response = requests.get(endpoints_cities.city_get)
        assert get_list_response.status_code == 200

    def create_city(self):
        body = {"name": self.name,
                "population": self.population}
        create_city_response = requests.post(endpoints_cities.city_post, json=body)
        self.id = create_city_response.json()['id']
        assert create_city_response.status_code == 201

    def get_city_name_by_id(self):
        get_city_response = requests.get(f'{endpoints_cities.city_get_id}/{self.id}')
        assert get_city_response.status_code == 200

    def update_city(self, new_name, new_population):
        body = {
            "name": new_name,
            "population": new_population
            }
        update_city_response = requests.put(f'{endpoints_cities.city_put}/{self.id}', json=body)
        self.name = update_city_response.json()['name']
        self.population = update_city_response.json()['population']
        assert update_city_response.status_code == 200

    def delete_city(self):
        delete_city_response = requests.delete(f'{endpoints_cities.city_delete}/{self.id}')
        assert delete_city_response.status_code == 204


if __name__ == '__main__':
    city = Cities()
    city.get_cities_list()
    print(city.name)
    print(city.population)
    city.create_city()
    print(city.id)
    city.get_city_name_by_id()
    city.update_city('wroo', 30303)
    city.delete_city()